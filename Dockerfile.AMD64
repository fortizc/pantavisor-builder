FROM docker:stable

WORKDIR /work

ENV REPO_REV=v2.9

RUN apk update && apk add \
    coreutils \
    curl \
    git \
    python2 \
    docker \
    bash \
    bash-completion \
    httpie \
    jq \
    sfdisk \
    xz \
    squashfs-tools \
    py-pip \
    groff \
    xmlstarlet \
    perl-xml-xpath \
    cryptsetup \
    tar \
    make \
    gcc \
    cpio \
    libncurses-dev \
    gawk \
    flex \
    bison \
    openssl \
    libssl-dev \
    dkms \
    libelf-dev \
    libudev-dev \
    libpci-dev \
    libiberty-dev \
    autoconf \
    llvm \
    && git config --global user.name "Aníbal Portero Hermida" \
    && git config --global user.email "anibal.portero@pantacor.com" \
    && pip install awscli \
    && curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo \
    && chmod a+x /usr/bin/repo \
    && repo init -u https://gitlab.com/pantacor/pv-manifest; repo sync scripts \
    && cp scripts/pvr/pvr /usr/bin/pvr \
    && pvr global-config DistributionTag=develop \
    && pvr self-upgrade

COPY pvr-sdk/files/usr/bin/pvcontrol /usr/bin/pvcontrol

WORKDIR /
RUN rm -rf /work
